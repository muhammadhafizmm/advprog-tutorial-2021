package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
    }

    //ToDo: Complete Me
    @Override
    public void update() {
        if (guild.getQuestType().equals("delivery") || guild.getQuestType().equals("rumble") || guild.getQuestType().equals("escort")) {
            getQuests().add(guild.getQuest());
        }
        
    }
}

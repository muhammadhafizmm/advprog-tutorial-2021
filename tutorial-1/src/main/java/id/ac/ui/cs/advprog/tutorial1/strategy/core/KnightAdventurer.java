package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    public KnightAdventurer() {
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }
    //ToDo: Complete me
    @Override
    public String getAlias() {
        return "Knight Adventurer";
    }
}

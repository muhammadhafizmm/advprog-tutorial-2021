package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }
    @Override
    public void update() {
        if (guild.getQuestType().equals("delivery") || guild.getQuestType().equals("escort")) {
            getQuests().add(guild.getQuest());
        }
    }
}

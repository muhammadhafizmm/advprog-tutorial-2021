package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    public ArrayList<Spell> spellArrayList;

    public ChainSpell(ArrayList<Spell> arraySpell){
        spellArrayList = arraySpell;
    }

    @Override
    public void cast() {
        for (Spell spell : spellArrayList){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = (spellArrayList.size() - 1); i >= 0; i--){
            spellArrayList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}

package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell prevSpell;
    private Spell curSpell;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        curSpell = spells.get(spellName);
        curSpell.cast();
        prevSpell = curSpell;
    }

    public void undoSpell() {
        if (prevSpell != null) { prevSpell.undo();}
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}

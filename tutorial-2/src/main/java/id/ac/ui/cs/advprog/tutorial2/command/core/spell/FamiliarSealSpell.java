package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSealSpell extends FamiliarSpell {
    public FamiliarSealSpell(Familiar spirit){
        super(spirit);
    }

    @Override
    public void cast() {
        super.familiar.seal();
    }

    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }
}
